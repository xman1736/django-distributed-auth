from django.urls import path
from dauth.views import GetTokenAPIView, TokenValidityAPIView

urlpatterns = [
    path('get-token/', GetTokenAPIView.as_view(), name='get-token'),
    path('validity/', TokenValidityAPIView.as_view(), name='token-validity'),
]
