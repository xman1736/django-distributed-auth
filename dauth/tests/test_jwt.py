import hashlib
import hmac
import json

from django.conf import settings
from rest_framework.test import APITestCase
from rest_framework import status
from django_redis import get_redis_connection


class NoDatabaseTestCase(APITestCase):
    databases = {}

    def tearDown(self):
        redis_connection = get_redis_connection("default")
        redis_key_1 = "user:1000001"
        redis_key_2 = "user:1000000"
        redis_connection.delete(redis_key_1, redis_key_2)
        super().tearDown()


class GetTokenAPITest(NoDatabaseTestCase):
    def test_get_token(self):
        url = '/api/get-token/'
        data = {'user_id': 1000001}

        message = json.dumps(data, separators=(',', ':')).encode('utf-8')
        calculated_hmac = hmac.new(settings.HMAC_KEY, message, hashlib.sha256).hexdigest()
        headers = {'Authorization': f'HMAC {calculated_hmac}'}

        response = self.client.post(url, data, format='json', headers=headers)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        first_token = response.data['token']

        response = self.client.post(url, data, format='json', headers=headers)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['token'], first_token)


class TokenValidityAPITest(NoDatabaseTestCase):
    def test_token_validity(self):
        user_id = 1000000
        url = f'/api/validity/?jwt_token={user_id}&user_id={user_id}'

        message = json.dumps({}, separators=(',', ':')).encode('utf-8')
        calculated_hmac_empty = hmac.new(settings.HMAC_KEY, message, hashlib.sha256).hexdigest()
        headers_empty = {'Authorization': f'HMAC {calculated_hmac_empty}'}

        response = self.client.get(url, headers=headers_empty)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        create_token_url = '/api/get-token/'
        create_token_data = {'user_id': user_id}

        message = json.dumps(create_token_data, separators=(',', ':')).encode('utf-8')
        calculated_hmac = hmac.new(settings.HMAC_KEY, message, hashlib.sha256).hexdigest()
        headers = {'Authorization': f'HMAC {calculated_hmac}'}

        create_token_response = self.client.post(create_token_url, create_token_data, format='json', headers=headers)
        self.assertEqual(create_token_response.status_code, status.HTTP_201_CREATED)

        url = f'/api/validity/?jwt_token={create_token_response.json()["token"]}&user_id={user_id}'
        response = self.client.get(url, headers=headers_empty)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(response.data['is_valid'])

        url = f'/api/validity/?jwt_token={create_token_response.json()["token"]}&user_id={user_id-200}'
        response = self.client.get(url, headers=headers_empty)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertFalse(response.data['is_valid'])
