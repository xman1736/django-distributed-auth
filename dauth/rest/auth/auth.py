import hashlib
import hmac
import json

from django.conf import settings
from rest_framework.authentication import BaseAuthentication
from rest_framework.exceptions import AuthenticationFailed


class HMACAuthentication(BaseAuthentication):
    def authenticate(self, request):
        if 'swagger' in request.path or 'redoc' in request.path:
            return None
        if 'Authorization' not in request.headers:
            raise AuthenticationFailed('No HMAC provided')

        try:
            provided_hmac = request.headers['Authorization'].split()[1].encode('utf-8')
        except (KeyError, IndexError):
            raise AuthenticationFailed('Invalid Authorization header format')

        message = json.dumps(request.data, separators=(',', ':')).encode('utf-8')

        calculated_hmac = hmac.new(self.get_secret_key(), message, hashlib.sha256).hexdigest()

        if not hmac.compare_digest(calculated_hmac.encode('utf-8'), provided_hmac):
            raise AuthenticationFailed('Invalid HMAC')

        return None

    def authenticate_header(self, request):
        return 'HMAC'

    @staticmethod
    def get_secret_key():
        return settings.HMAC_KEY
