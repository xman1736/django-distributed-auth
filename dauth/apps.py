import os

from django.apps import AppConfig
from django.conf import settings
from django.utils.translation import gettext_lazy as _


class DistributedAuthConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "dauth"
    verbose_name = _("Distributed auth")

    def ready(self):
        if not os.path.isdir(settings.STATIC_ROOT):
            os.makedirs(settings.STATIC_ROOT)
        if not os.path.isdir(settings.MEDIA_ROOT):
            os.makedirs(settings.MEDIA_ROOT)
