import json
import jwt

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from django_redis import get_redis_connection
from django.conf import settings
from datetime import datetime, timedelta

from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi


class GetTokenAPIView(APIView):
    @swagger_auto_schema(
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            required=['user_id'],
            properties={
                'user_id': openapi.Schema(type=openapi.TYPE_INTEGER)
            }
        ),
        responses={
            200: openapi.Schema(
                type=openapi.TYPE_OBJECT,
                properties={
                    'user_id': openapi.Schema(type=openapi.TYPE_INTEGER),
                    'token': openapi.Schema(type=openapi.TYPE_STRING),
                    'exp': openapi.Schema(type=openapi.TYPE_NUMBER),
                }
            ),
            201: openapi.Schema(
                type=openapi.TYPE_OBJECT,
                properties={
                    'user_id': openapi.Schema(type=openapi.TYPE_INTEGER),
                    'token': openapi.Schema(type=openapi.TYPE_STRING),
                    'exp': openapi.Schema(type=openapi.TYPE_NUMBER),
                }
            ),
            400: "Bad Request: Invalid input",
        },
        operation_description="Generate or retrieve JWT token for a user.",
    )
    def post(self, request, *args, **kwargs):
        user_id = request.data.get('user_id')

        if not user_id:
            return Response({'error': 'user_id is required'}, status=status.HTTP_400_BAD_REQUEST)

        redis_connection = get_redis_connection("default")
        redis_key = f"user:{user_id}"
        stored_data = redis_connection.get(redis_key)

        if stored_data:
            stored_data = json.loads(stored_data)
            stored_token = stored_data.get("token")
            expiration_time = stored_data.get("exp")
            if expiration_time:
                current_time = datetime.utcnow()
                expiration_datetime = datetime.fromtimestamp(expiration_time)
                time_difference = expiration_datetime - current_time

                if time_difference.total_seconds() > 0:
                    return Response({'user_id': user_id, 'token': stored_token, 'exp': expiration_time},
                                    status=status.HTTP_200_OK)

        new_expiration_time = datetime.utcnow() + timedelta(seconds=settings.EXPIRED_TIME_SEC)
        new_jwt_token = jwt.encode({'user_id': user_id, 'exp': new_expiration_time}, settings.SECRET_KEY, algorithm='HS256')

        redis_connection.set(redis_key, json.dumps({"token": new_jwt_token.decode('utf-8'), "exp": new_expiration_time.timestamp()}))

        return Response({'user_id': user_id, 'token': new_jwt_token.decode('utf-8'), 'exp': new_expiration_time.timestamp()},
                        status=status.HTTP_201_CREATED)


class TokenValidityAPIView(APIView):
    @swagger_auto_schema(
        manual_parameters=[
            openapi.Parameter('jwt_token', openapi.IN_QUERY, description="JWT token for validation",
                              type=openapi.TYPE_STRING),
            openapi.Parameter('user_id', openapi.IN_QUERY, description="User id for JWT for validation",
                              type=openapi.TYPE_STRING)
        ],
        responses={
            200: openapi.Schema(
                type=openapi.TYPE_OBJECT,
                properties={
                    'is_valid': openapi.Schema(type=openapi.TYPE_BOOLEAN),
                }
            ),
            400: "Bad Request: Invalid input",
            401: "Unauthorized: Token is invalid or has expired",
        },
        operation_description="Check if the JWT token is valid.",
    )
    def get(self, request, *args, **kwargs):
        jwt_token = request.query_params.get('jwt_token')
        user_id = request.query_params.get('user_id')

        if not jwt_token:
            return Response({'error': 'Missing jwt_token in query parameters'}, status=status.HTTP_400_BAD_REQUEST)

        try:
            decoded_token = jwt.decode(jwt_token, settings.SECRET_KEY, algorithms=['HS256'])
        except jwt.ExpiredSignatureError:
            return Response({'is_valid': False}, status=status.HTTP_200_OK)
        except jwt.InvalidTokenError:
            return Response({'is_valid': False}, status=status.HTTP_200_OK)

        if not user_id:
            return Response({'is_valid': False}, status=status.HTTP_200_OK)

        redis_connection = get_redis_connection("default")
        redis_key = f"user:{user_id}"
        stored_data = redis_connection.get(redis_key)

        if stored_data:
            stored_data = json.loads(stored_data)
            stored_token = stored_data.get("token")
            expiration_time = stored_data.get("exp")

            if expiration_time:
                current_time = datetime.utcnow()
                expiration_datetime = datetime.fromtimestamp(expiration_time)
                is_valid = expiration_datetime > current_time

                if is_valid and jwt_token != stored_token:
                    return Response({'is_valid': False}, status=status.HTTP_200_OK)
                return Response({'is_valid': is_valid}, status=status.HTTP_200_OK)

        return Response({'is_valid': False}, status=status.HTTP_200_OK)
