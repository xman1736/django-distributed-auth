import re

import os


from django.core.management import BaseCommand
from django.conf import settings

from dauth.management.commands.jwt_processing import JWTProcessor


class Command(BaseCommand):
    help = 'Test'

    def handle(self, *args, **kwargs):
        jwt_processor = JWTProcessor()
        jwt_processor.update_jwt_tokens()

