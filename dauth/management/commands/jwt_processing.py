import json
from datetime import datetime, timedelta

import jwt

from asgiref.sync import sync_to_async
from django_redis import get_redis_connection
from django.conf import settings

from .base_subprocess import Command as BaseCommand


class JWTProcessor(object):
    def update_jwt_tokens(self):
        redis_connection = get_redis_connection("default")

        token_keys = redis_connection.keys("user:*")

        for token_key in token_keys:
            stored_data = redis_connection.get(token_key)

            if stored_data:
                stored_data = json.loads(stored_data)
                stored_token = stored_data.get("token")
                expiration_time = stored_data.get("exp")

                current_time = datetime.utcnow()
                expiration_datetime = datetime.fromtimestamp(expiration_time)
                time_difference = current_time - expiration_datetime

                if time_difference.total_seconds() > 0:
                    try:
                        user_id = int(token_key.decode('utf-8').split(':')[1])
                        new_expiration_time = datetime.utcnow() + timedelta(seconds=settings.EXPIRED_TIME_SEC)

                        new_jwt_token = jwt.encode({'user_id': user_id, 'exp': new_expiration_time},
                                                   settings.SECRET_KEY, algorithm='HS256')

                        redis_connection.set(token_key, json.dumps(
                            {"token": new_jwt_token.decode('utf-8'), "exp": new_expiration_time.timestamp()}))
                    except jwt.InvalidTokenError:
                        print(f"Invalid token in key {token_key}.")

    def processing(self):
        self.update_jwt_tokens()


class Command(BaseCommand):
    help = 'Update JWT tokens'

    command_name = __name__.rsplit('.', 1)[-1]

    delay = 60

    jwt_processor = None

    @sync_to_async
    def processing(self):
        self.jwt_processor.processing()

    def before_start(self):
        self.jwt_processor = JWTProcessor()

    async def do_action(self, context):
        await self.processing()
